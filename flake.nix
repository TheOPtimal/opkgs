{
  description = "Things that aren't in nixpkgs";

  inputs = {
    # Nix packages collection
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";

    # Helper utilities
    flake-utils.url = "github:numtide/flake-utils";

    # Older Nix compatiblity
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    # Sources

    # Alacritty
    alacritty-ligatures = {
      url = "github:fee1-dead/alacritty/ligatures_harfbuzz";
      flake = false;
    };

    # Shell

    # Firefox addon dependency generator
    nixpkgs-firefox-addons = {
      url = "gitlab:rycee/nixpkgs-firefox-addons";
      inputs = {
        nixpkgs.follows = "nixpkgs-fix";
        flake-utils.follows = "flake-utils";
      };
    };

    # nixpkgs fix
    nixpkgs-fix.url = "nixpkgs/67c80531be622641b5b2ccc3a7aff355cb02476b";
  };

  outputs = {
    nixpkgs,
    flake-utils,
    alacritty-ligatures,
    nixpkgs-firefox-addons,
    ...
  }: let
    # System-independent nixpkgs library
    inherit (nixpkgs) lib;

    # Shorthand-ify 'flake-utils.lib'
    utils = flake-utils.lib;
  in
    utils.eachDefaultSystem (system: let
      # Generate nixpkgs for system
      pkgs = nixpkgs.legacyPackages.${system};

      # nixpkgs-firefox-addons package
      ff-addons = nixpkgs-firefox-addons.defaultPackage.${system};

      # Formatters and linters
      p-codestyle = pkgs.writeShellScriptBin "p-codestyle" ''
        shopt -s globstar extglob
        FILES='**/!(generated).nix'

        ${pkgs.deadnix}/bin/deadnix -e $FILES
        for i in **/!(generated).nix; do
          ${pkgs.statix}/bin/statix fix $i
        done
        ${pkgs.alejandra}/bin/alejandra $FILES
      '';

      # update
      p-update = pkgs.writeShellScriptBin "p-update" ''
        nix flake update

        cd ./packages/firefoxAddons
        nix develop -c nixpkgs-firefox-addons addons.json generated.nix
        cd ../..

        cd ./packages/vimPlugins/
        ./update_sources.sh
        cd ../..
      '';
    in {
      legacyPackages = {
        # Import alacritty package
        alacritty = pkgs.callPackage ./packages/alacritty {src = alacritty-ligatures;};

        # Import firefox addons
        firefoxAddons = pkgs.callPackage ./packages/firefoxAddons {};

        # Imports fonts
        fonts = pkgs.callPackage ./packages/fonts {};

        # Import mkPandoc helper
        mkPandoc = pkgs.callPackage ./packages/pandoc/mkPandoc.nix {};

        # Import pandoc mermaid filter
        pandoc-mermaid-filter = pkgs.python39Packages.callPackage ./packages/pandoc/mermaid-filter.nix {};

        # Import Pano GNOME Extension
        pano = pkgs.callPackage ./packages/pano/default.nix {};

        # Import qalculate-gtk override
        qalculate-gtk = pkgs.callPackage ./packages/qalculate-gtk/default.nix {};

        # Import skim override
        skim = pkgs.callPackage ./packages/skim {};

        # Import neovim plugins
        vimPlugins = pkgs.callPackage ./packages/vimPlugins {};

        # Project formatter
        inherit p-codestyle;
      };

      # Development shell with update tools
      devShell = pkgs.mkShell {
        packages = with pkgs; [
          ff-addons

          p-codestyle
          p-update
        ];
      };
    })
    // {
      lib = {
        concatFiles = files: builtins.concatLines (map builtins.readFile files);
      };

      templates = {
        pandoc-xelatex = {
          path = ./templates/pandoc-xelatex;
          description = "Build a markdown document with Pandoc and XeLaTeX";
        };
      };
    };
}
