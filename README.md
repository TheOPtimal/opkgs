# `opkgs` Package Repository

[![Built with Nix](https://img.shields.io/badge/Built%20With-Nix-blue?style=for-the-badge&logo=nixos&logoColor=9cf)](https://builtwithnix.org/) [![Fuck it - Ship it](https://forthebadge.com/images/badges/fuck-it-ship-it.svg)](https://forthebadge.com) [![Uses badges](https://forthebadge.com/images/badges/uses-badges.svg)](https://forthebadge.com) [![60% of the time - Works every time](https://forthebadge.com/images/badges/60-percent-of-the-time-works-every-time.svg)](https://forthebadge.com)

This repository contains custom packages and helpers that I use.

## Using it in a flake

```nix
...

inputs = {
	...

	opkgs = {
		url = "gitlab:TheOPtimal/opkgs";
		inputs = {
			nixpkgs.follows = "nixpkgs"; # If you already have nixpkgs as an input
			flake-utils.follows = "flake-utils"; # If you already have flake-utils as an input
		};
	};

	...
};

...
```

## Updating

To update the packages, just run the update script:

```bash
./update.sh
```

(Requires Nix 2.4 or later)
