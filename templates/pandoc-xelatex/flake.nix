{
  description = "Document description here";

  inputs = {
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";

    opkgs = {
      url = "gitlab:TheOPtimal/opkgs";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs = {
    nixpkgs,
    flake-utils,
    opkgs,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        pkgs = nixpkgs.legacyPackages."${system}";
        opkg = opkgs.legacyPackages."${system}";
      in rec {
        # `nix build`
        packages.document = opkg.mkPandoc {
          name = "document";
          src = ./.;
        };
        defaultPackage = packages.document;

        # `nix develop`
        devShell = pkgs.mkShell {
          nativeBuildInputs = with pkgs; with opkg; [pandoc];
        };
      }
    );
}
