{
  fetchgit,
  vimUtils,
  callPackage,
}: let
  # Get plugins JSON
  generated = builtins.fromJSON (builtins.readFile ./generated.json);

  plugins = builtins.mapAttrs (name: plugin:
    vimUtils.buildVimPlugin {
      pname = name;
      version = plugin.rev;
      src = fetchgit plugin;
    })
  generated;
in
  plugins
  // {
    skim-vim = callPackage ../skim/vim.nix {};
  }
