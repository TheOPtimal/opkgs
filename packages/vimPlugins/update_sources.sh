#! /usr/bin/env nix-shell
#! nix-shell -i bash -p bash curl nix-prefetch-git gnused jq

# Update plugin sources

# Not very safe - should be cleaner & could be more parallel
# should always be permitted to run to completion

in=plugins.yaml
out=generated.json

# Read the plugins.yaml
< $in \
sed -nE "s~^([-_[:alnum:]]+): *(.*)~\1 \2~p" \
| while read name src; do
	echo "{\"key\":\"$name\",\"value\":"
	nix-prefetch-git $src
	echo "}"
done \
| jq -s ".|del(.[].value.date)|del(.[].value.path)|from_entries" \
> $out
