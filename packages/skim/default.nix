{
  skim,
  stdenv,
}:
skim.overrideAttrs (oldAttrs: rec {
  postInstall =
    oldAttrs.postInstall
    + ''
      if ! [[ -f $out/bin/fzf ]]; then
        cat >> $out/bin/fzf << EOF
          #!${stdenv.shell}
          SK_DEFAULT_OPTS=$SK_DEFAULT_OPTS"\n$FZF_DEFAULT_OPTS" $out/bin/sk "$@"
      EOF
        chmod +x $out/bin/fzf
      fi
    '';
})
