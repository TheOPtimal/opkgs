{
  libqalculate,
  qalculate-gtk,
  gnuplot_qt,
}:
qalculate-gtk.override {
  libqalculate = libqalculate.override {
    gnuplot = gnuplot_qt;
  };
}
