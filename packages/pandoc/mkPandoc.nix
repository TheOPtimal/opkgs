{
  stdenv,
  lib,
  makeFontsConf,
  pandoc,
  texlive,
  dejavu_fonts,
}: {
  filters ? [],
  pandocExecutable ? "pandoc",
  fonts ? [dejavu_fonts],
  from ? "markdown",
  to ? "pdf",
  file ? "Document",
  documentName ? "${file}.${from}",
  resultName ? "${file}.${to}",
  extraPackages ? [],
  texlivePackage ? texlive.combined.scheme-small,
  basicArgs ? [],
  optArgs ?
    if to == "pdf"
    then {pdf-engine = "xelatex";}
    else {},
  ...
} @ inputs:
stdenv.mkDerivation (let
  # Get main executable of a program
  getMainProgram = package:
    package.meta.mainProgram or (package.pname or (lib.getName package.name));

  # Process the filters into arugments
  filterArgs =
    builtins.concatStringsSep " "
    (map (package: "--filter ${getMainProgram package}") filters);

  # Processed extra arguments
  optArgsProcessed =
    builtins.concatStringsSep " "
    (builtins.attrValues
      (builtins.mapAttrs
        (argName: argValue: "--${argName}=${argValue}")
        optArgs));

  # Processed basic arguments
  basicArgsProcessed = builtins.concatStringsSep " " basicArgs;

  # Font configuration
  fontConfig = makeFontsConf {fontDirectories = fonts;};
in
  {
    buildInputs = [pandoc] ++ filters ++ extraPackages ++ lib.optional (texlivePackage != null) texlivePackage;

    phases = ["buildPhase"];

    buildPhase = ''
      runHook preBuild

      export FONTCONFIG_FILE=${fontConfig}
      mkdir -p $out
      ${pandocExecutable} "$src/${documentName}" -f ${from} -t ${to} ${filterArgs} ${optArgsProcessed} ${basicArgsProcessed} -o $out/${resultName}

      runHook postBuild
    '';
  }
  // inputs)
