{
  lib,
  buildPythonPackage,
  fetchFromGitHub,
  pandocfilters,
  nodePackages,
}:
buildPythonPackage rec {
  pname = "pandoc-mermaid-filter";
  version = "0.1.0";

  src = fetchFromGitHub {
    owner = "oviquezr";
    repo = "pandoc-mermaid-filter";
    rev = "a663828ea69c9b5716e28ade897a8cae29863155";
    sha256 = "NjPtPSONGwbdSXfHQ55sdp1rbDuIZIk4yUslwNE96H4=";
  };

  propagatedBuildInputs = [pandocfilters];
  nativeBuildInputs = [nodePackages.mermaid-cli];

  postFixup = ''
    wrapPythonPrograms

    wrapProgram $out/bin/pandoc-mermaid --set MERMAID_BIN "${nodePackages.mermaid-cli}/bin/mmdc"
  '';

  meta = with lib; {
    description = "Pandoc filter for mermaid code blocks";
    homepage = "https://github.com/timofurrer/pandoc-mermaid-filter";
    license = licenses.mit;
    mainProgram = "pandoc-mermaid";
  };
}
