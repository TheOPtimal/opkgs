{
  fetchurl,
  lib,
  stdenv,
}: let
  # Build the addon itsself
  buildFirefoxXpiAddon = {
    pname,
    version,
    addonId,
    url,
    sha256,
    meta,
    ...
  }:
    stdenv.mkDerivation {
      # Set name
      name = "${pname}-${version}";

      inherit meta;

      # Fetch extension
      src = fetchurl {inherit url sha256;};

      preferLocalBuild = true;
      allowSubstitutes = false;

      # Move the download extension to the Firefox extension directory
      buildCommand = ''
        dst="$out/share/mozilla/extensions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}"
        mkdir -p "$dst"
        install -v -m644 "$src" "$dst/${addonId}.xpi"
      '';
    };

  # Import the generated packages and build the addons
  packages = import ./generated.nix {
    inherit fetchurl lib stdenv;
    inherit buildFirefoxXpiAddon;
  };
in
  packages
